import {NS} from "../../index";

export interface BBCronConfig {
  jobs: BBCronJob[];
}
export interface BBCronData {
  running: boolean;
  jobs: BBCronJob[];
}
export interface BBCronJob {
  file: string;
  freq: number;
  threads: number;
  enabled: boolean;
  last: number;
}

export class BBCronServer {
  config: BBCronConfig = {
    jobs: []
  };
  data: BBCronData = {
    running: false,
    jobs: []
  };
  constructor(public ns: NS) {
    ns.disableLog('sleep');

  }
  async start() {
    this.loadConfig();
    this.loadData();
    this.data.running = true;
    this.saveData();
    while (this.data.running) {
      await this.executeJobs();
      await this.ns.sleep(5);
    }
  }
  saveConfig() {
    localStorage.bbCronConfig = JSON.stringify(this.config);
  }
  saveData() {
    localStorage.bbCronData = JSON.stringify(this.data);
  }
  loadConfig() {
    if (localStorage.bbCronConfig) {
      this.config = JSON.parse(localStorage.bbCronConfig);
      if (!this.config.jobs) {
        this.config.jobs = [];
      }
    }
  }
  loadData() {
    if (localStorage.bbCronData) {
      this.data = JSON.parse(localStorage.bbCronData);
    }
  }
  async executeJobs() {
    this.loadConfig();
    let updated = false;
    let first = true;
    let proc = null;
    for (let job of this.config.jobs) {
      proc = this.ns.ps('home').find(p => p.filename == job.file);
      if (!proc && Date.now() > job.last + job.freq) {
        this.ns.run(job.file, 1);
        job.last = Date.now();
        updated = true;
      }
      if (first) {
        // await this.ns.sleep(50);
        first = false;
      }
    }
    if (updated) {
      this.saveData();
    }
    await this.ns.sleep(5);
  }
}

export async function listJobs(): Promise<BBCronJob[]> {
  let config: BBCronConfig = {jobs:[]};
  if (localStorage.bbCronConfig) {
    config = JSON.parse(localStorage.bbCronConfig);
  }
  return Promise.resolve(config.jobs);
}
export async function addJob(job: BBCronJob) {
  let config: BBCronConfig = {jobs:[]};
  if (localStorage.bbCronConfig) {
    config = JSON.parse(localStorage.bbCronConfig);
  }
  config.jobs.push(job);
  localStorage.bbCronConfig = JSON.stringify(config);
}
export async function rmJob(id: number) {
  let config: BBCronConfig = {jobs:[]};
  if (localStorage.bbCronConfig) {
    config = JSON.parse(localStorage.bbCronConfig);
  }
  config.jobs.splice(id, 1);
  localStorage.bbCronConfig = JSON.stringify(config);
}
