export class BBCronServer {
    constructor(ns) {
        this.ns = ns;
        this.config = {
            jobs: []
        };
        this.data = {
            running: false,
            jobs: []
        };
        ns.disableLog('sleep');
    }
    async start() {
        this.loadConfig();
        this.loadData();
        this.data.running = true;
        this.saveData();
        while (this.data.running) {
            await this.executeJobs();
            await this.ns.sleep(5);
        }
    }
    saveConfig() {
        localStorage.bbCronConfig = JSON.stringify(this.config);
    }
    saveData() {
        localStorage.bbCronData = JSON.stringify(this.data);
    }
    loadConfig() {
        if (localStorage.bbCronConfig) {
            this.config = JSON.parse(localStorage.bbCronConfig);
            if (!this.config.jobs) {
                this.config.jobs = [];
            }
        }
    }
    loadData() {
        if (localStorage.bbCronData) {
            this.data = JSON.parse(localStorage.bbCronData);
        }
    }
    async executeJobs() {
        this.loadConfig();
        let updated = false;
        let first = true;
        let proc = null;
        for (let job of this.config.jobs) {
            proc = this.ns.ps('home').find(p => p.filename == job.file);
            if (!proc && Date.now() > job.last + job.freq) {
                this.ns.run(job.file, 1);
                job.last = Date.now();
                updated = true;
            }
            if (first) {
                // await this.ns.sleep(50);
                first = false;
            }
        }
        if (updated) {
            this.saveData();
        }
        await this.ns.sleep(5);
    }
}
export async function listJobs() {
    let config = { jobs: [] };
    if (localStorage.bbCronConfig) {
        config = JSON.parse(localStorage.bbCronConfig);
    }
    return Promise.resolve(config.jobs);
}
export async function addJob(job) {
    let config = { jobs: [] };
    if (localStorage.bbCronConfig) {
        config = JSON.parse(localStorage.bbCronConfig);
    }
    config.jobs.push(job);
    localStorage.bbCronConfig = JSON.stringify(config);
}
export async function rmJob(id) {
    let config = { jobs: [] };
    if (localStorage.bbCronConfig) {
        config = JSON.parse(localStorage.bbCronConfig);
    }
    config.jobs.splice(id, 1);
    localStorage.bbCronConfig = JSON.stringify(config);
}
