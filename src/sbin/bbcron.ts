import {AutocompleteData, NS} from "../../index";
import {BBCronConfig, BBCronData, BBCronJob, BBCronServer, listJobs, addJob, rmJob} from "lib/libbbcron";

const cmds = ['start', 'stop', 'status', 'list', 'add', 'rm'];
export function autocomplete(data: AutocompleteData, args: string[]) {
  if (args.length === 0 || !cmds.includes(args[0])) {
    return cmds;
  }
  else if (args[0] === 'add' && args.length === 4) {
    return data.scripts;
  }
  else {
    return [];
  }
}

export function help(ns: NS) {
  ns.tprint('BBCron Help');
  ns.tprint('run /sbin/bbcron.js <cmd> [args...]');
  ns.tprint('Commands');
  ns.tprint('start = Start BBCron server');
  ns.tprint('stop = Stop BBCron server');
  ns.tprint('status = Show BBCron server status');
  ns.tprint('list = Show list of cron jobs');
  ns.tprint('add <freq> <threads> <script> = Add a new cron job');
  ns.tprint('rm <id> = Remove cron job by ID');
}

export async function main(ns: NS) {
  if (ns.args[0] === 'start') {
    const bbCronServer = new BBCronServer(ns);
    await bbCronServer.start();
  }
  else if (ns.args[0] === 'stop') {
    ns.scriptKill('/sbin/bbcron.js', 'home');
  }
  else if (ns.args[0] === 'status') {
    let bbCronConfig: BBCronConfig = (localStorage.bbCronConfig?JSON.parse(localStorage.bbCronConfig):{});
    let bbCronData: BBCronData = (localStorage.bbCronData?JSON.parse(localStorage.bbCronData):{});
    if (!ns.isRunning('/sbin/bbcron.js', 'home', 'start')) {
      bbCronData.running = false;
      localStorage.bbCronData = JSON.stringify(bbCronData);
    }
    ns.tprint(`INFO BBCron Running(${bbCronData.running})`);
  }
  else if (ns.args[0] === 'list') {
    const jobs = await listJobs();
    for (let jobId in jobs) {
      let job = jobs[jobId];
      ns.tprint(
        `Job(${jobId}) Enabled(${job.enabled}) Frequency(${job.freq}) Threads(${job.threads}) File(${job.file})`
      );
    }
  }
  else if (ns.args[0] === 'add') {
    if (ns.args.length === 4) {
      const newJob: BBCronJob = {
        freq: ns.args[1] as number,
        threads: ns.args[2] as number,
        file: ns.args[0] as string,
        last: 0,
        enabled: true
      };
      await addJob(newJob);
    } else {
      help(ns);
    }
  }
  else if (ns.args[0] === 'rm') {
    if (ns.args.length === 2) {
      await rmJob(ns.args[1] as number);
    } else {
      help(ns);
    }
  }
}
