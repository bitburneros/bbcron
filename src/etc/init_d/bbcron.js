const cmds = ['start', 'stop', 'status'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else {
        return [];
    }
}
export async function main(ns) {
    if (ns.args[0] === 'start') {
        ns.run('/sbin/bbcron.js', 1, 'start');
    }
    else if (ns.args[0] === 'stop') {
        ns.run('/sbin/bbcron.js', 1, 'stop');
    }
    else if (ns.args[0] === 'status') {
        ns.run('/sbin/bbcron.js', 1, 'status');
    }
}
